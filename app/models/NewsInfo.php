<?php
namespace Dkm\Models;

use Phalcon\Mvc\Model;

class NewsInfo extends Model
{
	public $id;
	public $title;
	public $brief_content;
	public $full_content;
	public $image;
	public $last_update;
}