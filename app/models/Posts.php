<?php
namespace Dkm\Models;

use Phalcon\Mvc\Model;

class Posts extends Model
{
	public $id;
	public $title;
	public $slug;
	public $content;
	public $image;
	public $lampiran;
	public $created;
	public $updated;

	public function initialize(){
		$this->belongsTo('post_category_id', 'Dkm\Models\PostCategories', 'id', [
			'alias' => 'category',
		]);
	}
}