<?php

namespace Dkm\Models;

use Phalcon\Mvc\Model;

class Category extends Model {

    public $id;
    public $name;

    public function initialize() {
        $this->hasMany('id', 'Dkm\Models\Mosque', 'category_id', [
            'alias' => 'mosques',
        ]);
    }

}
