<?php
namespace Dkm\Models;

use Phalcon\Mvc\Model;

class Images extends Model
{
	public $place_id;
	public $name;

	public function initialize(){
		$this->belongsTo('place_id', 'Dkm\Models\Place', 'place_id', [
			'alias' => 'place',
		]);
	}
}