<?php
namespace Dkm\Models;

use Phalcon\Mvc\Model;

class PostCategories extends Model
{
	public $id;
	public $name;
	public $created;
	public $updated;

	public function initialize(){
		$this->hasMany('id', 'Dkm\Models\Posts', 'post_category_id', [
			'alias' => 'posts',
		]);
	}
}