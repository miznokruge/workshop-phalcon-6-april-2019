<?php
namespace Dkm\Models;

use Phalcon\Mvc\Model;

class MosqueFacilities extends Model
{
	public $id;
	public $user_id;
	public $mosque_id;
	public $facility_id;
	public $created_at;
	public $updated_at;

	public function initialize(){
		$this->belongsTo('user_id', 'Dkm\Models\Users', 'id', [
			'alias' => 'user',
		]);

		$this->belongsTo('mosque_id', 'Dkm\Models\Mosques', 'id', [
			'alias' => 'mosque',
		]);

		$this->belongsTo('facility_id', 'Dkm\Models\Facilities', 'id', [
			'alias' => 'facility',
		]);
	}
}