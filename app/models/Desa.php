<?php

namespace Dkm\Models;

use Phalcon\Mvc\Model;

class Desa extends Model {

    public $id;
    public $slug;
    public $name;
    public function initialize() {
        $this->belongsTo('id_kecamatan', 'Dkm\Models\Kecamatan', 'id', [
            'alias' => 'kecamatan',
        ]);
    }

}
