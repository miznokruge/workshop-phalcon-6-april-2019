<?php
namespace Dkm\Models;

use Phalcon\Mvc\Model;

class EventTypes extends Model
{
	public $id;
	public $type;

	public function initialize(){
		$this->hasMany('id', 'Dkm\Models\Events', 'event_type_id', [
			'alias' => 'events',
		]);
	}
}