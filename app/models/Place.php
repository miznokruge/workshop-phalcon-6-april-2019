<?php
namespace Dkm\Models;

use Phalcon\Mvc\Model;

class Place extends Model
{
	public $place_id;
	public $name;
	public $image;
	public $address;
	public $phone;
	public $website;
	public $description;
	public $lat;
	public $lng;
	public $last_update;

	public function initialize(){
		$this->hasMany('place_id', 'Dkm\Models\PlaceCategory', 'place_id', [
			'alias' => 'place_category',
		]);

		$this->hasMany('place_id', 'Dkm\Models\Image', 'place_id', [
			'alias' => 'image',
		]);
	}
}