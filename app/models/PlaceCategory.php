<?php
namespace Dkm\Models;

use Phalcon\Mvc\Model;

class PlaceCategory extends Model
{
	public $place_id;
	public $cat_id;

	public function initialize(){
		$this->belongsTo('place_id', 'Dkm\Models\Place', 'place_id', [
			'alias' => 'place',
		]);

		$this->belongsTo('cat_id', 'Dkm\Models\Category', 'cat_id', [
			'alias' => 'category',
		]);
	}
}