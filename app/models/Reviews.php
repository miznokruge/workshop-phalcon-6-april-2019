<?php

namespace Dkm\Models;

use Phalcon\Mvc\Model;

class Reviews extends Model {

    public $id;

    public function initialize() {
        $this->belongsTo('mosque_id', 'Dkm\Models\Mosque', 'id', [
            'alias' => 'mosque',
        ]);
    }

}
