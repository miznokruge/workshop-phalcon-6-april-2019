<?php
namespace Dkm\Models;

use Phalcon\Mvc\Model;

class Facilities extends Model
{
	public $id;
	public $name;

	public function initialize(){
		$this->hasMany('id', 'Dkm\Models\MosqueFacilities', 'facility_id', [
			'alias' => 'facilities',
		]);
	}
}