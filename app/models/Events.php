<?php
namespace Dkm\Models;

use Phalcon\Mvc\Model;

class Events extends Model
{
	public $id;
	public $event_type_id;
	public $title;
	public $description;
	public $start_date;
	public $end_date;
	public $photo;
	public $created_at;
	public $user_id;

	public function initialize(){
		$this->belongsTo('event_type_id', 'Dkm\Models\EventTypes', 'id', [
			'alias' => 'event_type',
		]);
	}
}