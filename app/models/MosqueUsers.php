<?php

namespace Dkm\Models;

use Phalcon\Mvc\Model;

class MosqueUsers extends Model {

    public $id;
    public $user_id;
    public $mosque_id;
    public $mosque_user_type_id;


    public function initialize() {
        $this->belongsTo('user_id', 'Dkm\Models\Users', 'id', [
            'alias' => 'user',
        ]);

        $this->belongsTo('mosque_id', 'Dkm\Models\Mosque', 'id', [
            'alias' => 'mosque',
        ]);

        $this->belongsTo('mosque_user_type_id', 'Dkm\Models\MosqueUserTypes', 'id', [
            'alias' => 'role',
        ]);
    }

}
