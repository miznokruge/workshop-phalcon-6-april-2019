<?php

namespace Dkm\Models;

use Phalcon\Mvc\Model;

class MosqueUserTypes extends Model {

    public $id;
    public $name;
    public $description;


    public function initialize() {
        $this->hasMany('id', 'Dkm\Models\MosqueUsers', 'mosque_user_id', [
            'alias' => 'users',
        ]);
    }

}
