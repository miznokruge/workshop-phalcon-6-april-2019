<?php

namespace Dkm\Models;

use Phalcon\Mvc\Model;

class Mosque extends Model {

    public $id;
    public $user_id;
    public $mosque_id;
    public $description;


    public function initialize() {
        $this->belongsTo('mosque_id', 'Dkm\Models\Mosque', 'id', [
            'alias' => 'mosque',
        ]);
        $this->belongsTo('user_id', 'Dkm\Models\User', 'id', [
            'alias' => 'user',
        ]);
    }

}
