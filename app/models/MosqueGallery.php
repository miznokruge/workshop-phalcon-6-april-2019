<?php

namespace Dkm\Models;

use Phalcon\Mvc\Model;

class MosqueGallery extends Model {

    public $id;
    public $mosque_id;
    public $photo;
    public $created_at;
    public $updated_at;

    public function initialize() {
        $this->belongsTo('mosque_id', 'Dkm\Models\Mosque', 'id', [
            'alias' => 'mosque',
        ]);
    }
}
