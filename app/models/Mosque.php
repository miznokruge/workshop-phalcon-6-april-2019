<?php

namespace Dkm\Models;

use Phalcon\Mvc\Model;

class Mosque extends Model {

    public $id;
    public $category_id;
    public $slug;
    public $name;
    public $location;
    public $image;
    public $createdated;
    public $updated;
    public $mosuqe_views;
    public $description;

    public function beforeValidationOnCreate() {
        $this->created = date('Y-m-d H:i:s');
        $this->updated = date('Y-m-d H:i:s');
    }

    public function initialize() {
        $this->belongsTo('category_id', 'Dkm\Models\Category', 'id', [
            'alias' => 'category',
        ]);
        
        $this->hasMany('id', 'Dkm\Models\Reviews', 'mosque_id', [
            'alias' => 'reviews',
        ]);

        $this->hasMany('id', 'Dkm\Models\MosqueGallery', 'mosque_id', [
            'alias' => 'gallery',
        ]);

        $this->hasMany('id', 'Dkm\Models\MosqueFacilities', 'mosque_id', [
            'alias' => 'facilities',
        ]);
    }

}
