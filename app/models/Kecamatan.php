<?php

namespace Dkm\Models;

use Phalcon\Mvc\Model;

class Kecamatan extends Model {

    public $id;
    public $slug;
    public $name;

    public function initialize() {
        $this->belongsTo('id_kabupaten', 'Dkm\Models\Kota', 'id', [
            'alias' => 'kota',
        ]);
    }

}
