<?php
namespace Dkm\Models;

use Phalcon\Mvc\Model;

class Gcm extends Model
{
	public $id;
	public $device;
	public $email;
	public $version;
	public $regid;
	public $date_created;
}