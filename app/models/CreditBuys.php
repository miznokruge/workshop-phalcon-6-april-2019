<?php

namespace Dkm\Models;

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;

class CreditBuys extends Model {

    public $id;
    public $user_id;
    public $status;
    public $code;
    public $amount;
    public $price;

}
