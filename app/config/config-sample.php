<?php

use Phalcon\Config;
use Phalcon\Logger;

return new Config([
    'database' => [
        'adapter' => 'Mysql',
        'host' => '127.0.0.1',
        'username' => 'root',
        'password' => 'bitnami',
        'dbname' => 'project_dkm_or_id'
    ],
    'application' => [
        'controllersDir' => APP_PATH . '/controllers/',
        'modelsDir' => APP_PATH . '/models/',
        'formsDir' => APP_PATH . '/forms/',
        'viewsDir' => APP_PATH . '/views/',
        'libraryDir' => APP_PATH . '/library/',
        'pluginsDir' => APP_PATH . '/plugins/',
        'cacheDir' => BASE_PATH . '/cache/',
        'baseUri' => '/',
        'publicUrl' => 'http://skripsi.co.id-web.project.gaivo',
        'cryptSalt' => 'eEAfR|_&G&f,+vU]:jFr!!A&+71w1Ms9~8_4L!<@[N@DyaIP_2My|:+.u>/6m,$D',
        'env' => 'local',
        'debug' => true
    ],
    'mail' => [
        'fromName' => 'xx',
        'fromEmail' => 'xx',
        'smtp' => [
            'server' => 'xx',
            'port' => 587,
            'security' => 'tls',
            'username' => 'xx',
            'password' => 'xx'
        ]
    ],
    'amazon' => [
        'AWSAccessKeyId' => '',
        'AWSSecretKey' => ''
    ],
    'logger' => [
        'path' => BASE_PATH . '/logs/',
        'format' => '%date% [%type%] %message%',
        'date' => 'D j H:i:s',
        'logLevel' => Logger::DEBUG,
        'filename' => 'application.log',
    ],
    'google' => [
        'map_api_key' => 'xxx'
    ],
    // Set to false to disable sending emails (for use in test environment)
    'useMail' => true
        ]);
