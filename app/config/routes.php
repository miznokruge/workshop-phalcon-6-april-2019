<?php

/*
 * Define custom routes. File gets included in the router service definition.
 */
$router = new Phalcon\Mvc\Router();
$router->add('/profile', array('controller' => 'profiles', 'action' => 'index'));
$router->add('/login', array('controller' => 'index', 'action' => 'loginn'));
$router->add('/logout', array('controller' => 'session', 'action' => 'logout'));
$router->add('/profile.html', array('controller' => 'profiles', 'action' => 'index'));
$router->add('/about.html', array('controller' => 'index', 'action' => 'about'));
$router->add('/privacy-policy.html', array('controller' => 'index', 'action' => 'privacy_policy'));
$router->add('/cara-kerja.html', array('controller' => 'index', 'action' => 'cara_kerja'));
$router->add('/landing-signup', array('controller' => 'index', 'action' => 'landing_signup'));
$router->add('/term-of-use.html', array('controller' => 'index', 'action' => 'term_of_use'));
$router->add('/confirm/{code}/{email}', ['controller' => 'user_control', 'action' => 'confirmEmail']);
$router->add('/reset-password/{code}/{email}', ['controller' => 'user_control', 'action' => 'resetPassword']);
$router->add('/contact', ['controller' => 'index', 'action' => 'contact']);
$router->add('/books/([0-9]+)/([0-9a-z\-]+)', ['controller' => 'books', 'action' => 'viewbooks', 'id' => 1, 'slug' => 2]);
$router->add('/books/([0-9]+)', ['controller' => 'books', 'action' => 'viewbooks', 'id' => 1]);
$router->add('/viewbooks_list', ['controller' => 'books', 'action' => 'viewbooks_list']);
$router->add('/viewbooks_category/{cid}', ['controller' => 'books', 'action' => 'viewbooks_category']);
$router->add('/sitemap.xml', array('controller' => 'sitemap', 'action' => 'index'));
$router->add('/sitemap/([0-9a-z\-]+)\.xml', array('controller' => 'sitemap', 'action' => 1));
$router->add('/sitemap/([0-9a-z\-]+)/([0-9a-z\-]+)\.xml', array('controller' => 'sitemap', 'action' => 1, 'slug' => 2));
$router->add('/sitemap/([0-9a-z\-]+)/([0-9a-z\-]+)/([0-9a-z\-]+)\.xml', array('controller' => 'sitemap', 'action' => 1, 'slug' => 2, 'var' => 3));

$router->add('/berita', ['controller' => 'blogs', 'action' => 'index']);
$router->add('/berita/([0-9]+)/([0-9a-z\-]+)\.html', ['controller' => 'blogs', 'action' => 'view', 'id' => 1, 'slug' => 2]);

#DKM
$router->add('/dkm', ['controller' => 'dkm', 'action' => 'index']);
$router->add('/dkm/([0-9]+)/([0-9a-z\-]+)\.html', ['controller' => 'dkm', 'action' => 'view', 'id' => 1, 'slug' => 2]);
$router->add('/dashboard/addlisting.html', ['controller' => 'dashboard', 'action' => 'addlisting']);

#category
$router->add('/category', ['controller' => 'category', 'action' => 'index']);
$router->add('/category/([0-9]+)/([0-9a-z\-]+)\.html', ['controller' => 'category', 'action' => 'view', 'id' => 1, 'slug' => 2]);


$router->add('/jurusan', ['controller' => 'jurusan', 'action' => 'index']);
$router->add('/jurusan/([0-9]+)/([0-9a-z\-]+)', ['controller' => 'jurusan', 'action' => 'view', 'id' => 1, 'slug' => 2]);

return $router;
