<?php

use Phalcon\Loader;

$loader = new Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerNamespaces([
    'Dkm\Models'      => $config->application->modelsDir,
    'Dkm\Controllers' => $config->application->controllersDir,
    'Dkm\Forms'       => $config->application->formsDir,
    'Dkm'             => $config->application->libraryDir
]);

if ($config->application->debug == true) {
    $namespaces = array_merge($loader->getNamespaces(), array(
        'PDW' => realpath($config->application->libraryDir . '/PDW')
    ));
    $loader->registerNamespaces($namespaces);
}

$loader->register();

// Use composer autoloader to load vendor classes
require_once BASE_PATH . '/vendor/autoload.php';