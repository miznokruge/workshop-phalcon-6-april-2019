<?php

use Phalcon\Mvc\View;
use Phalcon\Crypt;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Files as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Direct as Flash;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Logger\Formatter\Line as FormatterLine;
use Dkm\Auth\Auth;
use Dkm\Acl\Acl;
use Dkm\Mail\Mail;
use Dkm\Util\Util;
use Phalcon\Flash\Session as FlashSession;
use Dkm\Util\Mosque as MosqueUtil;

/**
 * Register the global configuration as config
 */
$di->setShared('config', function () {
    $config = include APP_PATH . '/config/config.php';

    if (is_readable(APP_PATH . '/config/config.dev.php')) {
        $override = include APP_PATH . '/config/config.dev.php';
        $config->merge($override);
    }

    return $config;
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);
    return $url;
});

/**
 * Setting up the view component
 */
$di->set('view', function () {
    $config = $this->getConfig();

    $view = new View();
    $view->setViewsDir($config->application->viewsDir);
    return $view;
}, true);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function () {
    $config = $this->getConfig();
    return new DbAdapter([
        'host' => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname' => $config->database->dbname
    ]);
});

$di->set('dbblog', function () {
    $config = $this->getConfig();
    $connection = new DbAdapter(array(
        'host' => $config->dbblog->host,
        'username' => $config->dbblog->username,
        'password' => $config->dbblog->password,
        'dbname' => $config->dbblog->dbname
    ));
    return $connection;
}
);

$di->set('cache', function() {
    $config = $this->getConfig();
    /*
      if ('live' === $config->application->env) {
      $lredis = new \Illuminate\Redis\Database($config->redis->toArray());
      $store_cache = new \Illuminate\Cache\RedisStore($lredis, 'app_');
      } else {
      $file = new \Illuminate\Filesystem\Filesystem();
      $store_cache = new \Illuminate\Cache\FileStore($file, dirname(dirname(__FILE__)) . '/cache');
      }
     *
     */
    $file = new \Illuminate\Filesystem\Filesystem();
    $store_cache = new \Illuminate\Cache\FileStore($file, dirname(dirname(__FILE__)) . '/cache');
    return new \Illuminate\Cache\Repository($store_cache);
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->set('modelsMetadata', function () {
    $config = $this->getConfig();
    return new MetaDataAdapter([
        'metaDataDir' => $config->application->cacheDir . 'metaData/'
    ]);
});

/**
 * Start the session the first time some component request the session service
 */
$di->set('session', function () {
    $session = new SessionAdapter();
    $session->start();
    return $session;
});

$di->set('Util', function () {
    return new Util();
});

/**
 * Crypt service
 */
$di->set('crypt', function () {
    $config = $this->getConfig();

    $crypt = new Crypt();
    $crypt->setKey($config->application->cryptSalt);
    return $crypt;
});

/**
 * Dispatcher use a default namespace
 */
$di->set('dispatcher', function() use ($di) {
    $evManager = $di->getShared('eventsManager');

    $evManager->attach(
            "dispatch:beforeException", function($event, $dispatcher, $exception) {
        switch ($exception->getCode()) {
            case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
            case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                $dispatcher->forward(
                        array(
                            'controller' => 'error',
                            'action' => 'show404',
                        )
                );
                return false;
        }
    }
    );
    $dispatcher = new Dispatcher();
    $dispatcher->setEventsManager($evManager);
    $dispatcher->setDefaultNamespace('Dkm\Controllers');
    return $dispatcher;
}, true
);

/**
 * Loading routes from the routes.php file
 */
$di->set('router', function () {
    return require APP_PATH . '/config/routes.php';
});

/**
 * Flash service with custom CSS classes
 */
$di->set('flash', function () {
    return new Flash([
        'error' => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice' => 'alert alert-info',
        'warning' => 'alert alert-warning'
    ]);
});

$di->set('flashSession', function () {
    return new FlashSession(array(
        'error' => 'alert alert-danger flash',
        'success' => 'alert alert-success flash',
        'warning' => 'alert alert-warning flash',
        'notice' => 'alert alert-info flash'
    ));
});

/**
 * Custom authentication component
 */
$di->set('auth', function () {
    return new Auth();
});

/**
 * Mail service uses AmazonSES
 */
$di->set('mail', function () {
    return new Mail();
});

/**
 * Setup the private resources, if any, for performance optimization of the ACL.
 */
$di->setShared('AclResources', function() {
    $pr = [];
    if (is_readable(APP_PATH . '/config/privateResources.php')) {
        $pr = include APP_PATH . '/config/privateResources.php';
    }
    return $pr;
});

/**
 * Access Control List
 * Reads privateResource as an array from the config object.
 */
$di->set('acl', function () {
    $acl = new Acl();
    $pr = $this->getShared('AclResources')->privateResources->toArray();
    $acl->addPrivateResources($pr);
    return $acl;
});

/**
 * Logger service
 */
$di->set('logger', function ($filename = null, $format = null) {
    $config = $this->getConfig();

    $format = $format ?: $config->get('logger')->format;
    $filename = trim($filename ?: $config->get('logger')->filename, '\\/');
    $path = rtrim($config->get('logger')->path, '\\/') . DIRECTORY_SEPARATOR;

    $formatter = new FormatterLine($format, $config->get('logger')->date);
    $logger = new FileLogger($path . $filename);

    $logger->setFormatter($formatter);
    $logger->setLogLevel($config->get('logger')->logLevel);

    return $logger;
});

$di->set('modelsManager', function() {
    return new Phalcon\Mvc\Model\Manager();
});

$di->set('mail', function () {
    return new Dkm\Mail\Mail();
});


$di->set('Mosque', function () {
    return new MosqueUtil();
});