<?php

namespace Dkm\Util;

class Util extends \Phalcon\Mvc\User\Component {

    public static function hitung($a, $b) {
        return $a + $b;
    }

    public function getFirstName($name) {
        $a = explode(' ', $name);
        if (is_array($a)) {
            return $a[0];
        } else {
            return $name;
        }
    }

    public function debug($var = null, $end = false) {
        echo '<pre>';
        print_r($var);
        echo '</pre>';
        if ($end) {
            die;
        }
    }

    public function SeoUrl($str, $x = false) {
        if ($str !== mb_convert_encoding(mb_convert_encoding($str, 'UTF-32', 'UTF-8'), 'UTF-8', 'UTF-32'))
            $str = mb_convert_encoding($str, 'UTF-8', mb_detect_encoding($str));
        $str = htmlentities($str, ENT_NOQUOTES, 'UTF-8');
        $str = preg_replace('`&([a-z]{1,2})(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);`i', '\1', $str);
        $str = html_entity_decode($str, ENT_NOQUOTES, 'UTF-8');
        $str = preg_replace(array('`[^a-z0-9]`i', '`[-]+`'), '-', $str);
        $str = strtolower(trim($str, '-'));
        if ($x == true) {
            return $str;
        } else {
            return $str . '.html';
        }
    }

}
