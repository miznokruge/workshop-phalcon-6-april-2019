<?php

namespace Dkm\Util;

use Dkm\Models\Mosque as MosqueModel;

class Mosque extends \Phalcon\Mvc\User\Component {

    public function getCurrent() {
        return $this->session->get('current_mosque');
    }

    public function checkCurrent() {
        return $this->session->has('current_mosque');
    }
    
    public function getCurrentRole() {
        return $this->session->get('current_role');
    }

    public function checkCurrentRole() {
        return $this->session->has('current_role');
    }

    public function setCurrent($id,$role_id) {
        $this->session->remove('current_role');
        $this->session->remove('current_mosque');
        sleep(1);
        $mo = MosqueModel::findFirstByid($id);
        $role = \Dkm\Models\MosqueUserTypes::findFirstByid($role_id);
        $this->session->set('current_mosque', $mo);
        $this->session->set('current_role', $role);
        return true;
    }

}
