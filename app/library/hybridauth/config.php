<?php

/**
 * HybridAuth
 * http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
 * (c) 2009-2015, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
 */
// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------
return
        array(
            "base_url" => 'https://' . $_SERVER['HTTP_HOST'] . "/auth",
            "providers" => array(
                // openid providers
                "OpenID" => array(
                    "enabled" => true
                ),
                "LinkedIn" => array(
                    "enabled" => true,
                    "keys" => array(
                        "id" => "75tqidlf40mch9",
                        "key" => "75tqidlf40mch9",
                        "secret" => "ulo1C3DEVowUP9XL")
                ),
                "Google" => array(
                    "enabled" => true,
                    "keys" => array(
                        "id" => "521531264466-iaqm7vrs7445nl24u9otlfune2976uid.apps.googleusercontent.com", 
                        "secret" => "HtK9UGlCVjnrvm0q474RhxYu"),
                    "scope" => "https://www.googleapis.com/auth/userinfo.profile " . // optional
                    "https://www.googleapis.com/auth/userinfo.email", // optional
                    "access_type" => "offline", // optional
                    "approval_prompt" => "force", // optional
                    "hd" => "dkm.or.id" // optional
                ),
//                "Facebook" => array(
//                    "enabled" => true,
//                    "keys" => array(
//                        "id" => "136517743182994",
//                        "secret" => "7cf350800ceb97ee4c9fe3ce7f9c8854"
//                    ),
//                    'scope' => 'email, user_about_me, user_birthday, user_hometown, user_location, user_website',
//                    'trustForwarded' => true
//                ),
                "Facebook" => array(
                    "enabled" => true,
                    "keys" => array(
                        "id" => "1742296249301655",
                        "secret" => "fd2c8c5fcd280fe82a35b1971b8d5f92"
                        ),
                    'scope' => 'email, public_profile',
                    'trustForwarded' => true
                ),
                "Twitter" => array(
                    "enabled" => true,
                    "keys" => array(
                        "key" => "cxNppDmA8XWGFiawDiYjTb7Bo",
                        "secret" => "u0g6lFujZjwjlpHhhoQAQ6LeIx7cCtYm7myXN9vgZHxPaLcqEz")
                ),
                "Yahoo" => array(
                    "enabled" => true,
                    "keys" => array(
                        "id" => "dj0yJmk9MnNXdzEwRkgyamFNJmQ9WVdrOWNqQkNXVUkwTjJrbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD0xMQ--",
                        "secret" => "291fbe6dd9a3dcd4e35ef5b01ce04b752dc44712"),
                ),
                "AOL" => array(
                    "enabled" => true
                ),
                // windows live
                "Live" => array(
                    "enabled" => true,
                    "keys" => array("id" => "", "secret" => "")
                ),
                "MySpace" => array(
                    "enabled" => true,
                    "keys" => array("key" => "", "secret" => "")
                ),
                "Foursquare" => array(
                    "enabled" => true,
                    "keys" => array("id" => "", "secret" => "")
                ),
            ),
            // if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
            "debug_mode" => false,
            "debug_file" => "log.txt",
);
