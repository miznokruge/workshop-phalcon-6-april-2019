<?php

namespace Dkm\Controllers;

use Dkm\Util\Util;
use Dkm\Models\Category;
use Dkm\Models\Posts;
use Dkm\Models\Mosque;
use Phalcon\Mvc\Url;
use Phalcon\Paginator\Adapter\Model as Paginator;

/**
 * Display the default index page.
 */
class ConsoleController extends ControllerBase {

    public function initialize() {
        if (!is_array($this->auth->getIdentity())) {
            return $this->response->redirect('/');
        }
        //jika tidak punya masjid,arahkan ke halaman user biasa
        if (count($this->auth->getIdentity()['mosques']) <= 0) {
            return $this->response->redirect('/dashboard');
        }
        if (false === $this->Mosque->checkCurrent()) {
            $this->Mosque->setCurrent($user['mosques'][0]['mosque']['id'],$user['mosques'][0]['role']['id']);
            sleep(1);
        }
        $this->view->setTemplateBefore('pengurus');
        $this->view->mosque = $this->Mosque->getCurrent();
    }
    
    
     public function usersAction() {         
         $users= \Dkm\Models\MosqueUsers::find(["mosque_id='".$this->Mosque->getCurrent()->id."'"]);
         $this->view->users=$users;
     }
    

    public function csAction() {
        $this->debug($this->auth->getIdentity());
        die;
    }

    public function indexAction() {        
        $user = $this->auth->getIdentity();        
        $this->tag->setTitle("Dashboard | " . $mosque->name . "");        
    }

    public function set_currentAction($id,$role_id) {
        $this->Mosque->setCurrent($id,$role_id);
        return $this->response->redirect('/console');
    }

    public function get_currentAction() {
        //$this->debug((array)$this->Mosque->getCurrent());
        $this->debug((array)$this->Mosque->getCurrentRole());
        die;
    }

}
