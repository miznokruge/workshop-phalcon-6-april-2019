<?php

namespace Dkm\Controllers;

class SitemapController extends ControllerBase {

    protected $domain = 'https://skripsi.co.id';

    public function initialize() {
//for big files, to be on the safe side
        set_time_limit(0);
    }

    public function indexAction() {
        $response = new \Phalcon\Http\Response();
        $expireDate = new \DateTime();
        $expireDate->modify('+1 day');
        $response->setExpires($expireDate);
        $response->setHeader('Content-Type', "application/xml; charset=UTF-8");
        $links = array(
            array('u' => 'pages'),
            array('u' => 'categories'),
            array('u' => 'authors'),
            array('u' => 'books'),
            array('u' => 'majors'),
            array('u' => 'blog'),
        );
        $data = '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        foreach ($links as $l) {
            $data .= '<sitemap>';
            $data .= '<loc>' . $this->domain . '/sitemap/' . $l['u'] . '.xml</loc>';
            $data .= '</sitemap>';
        }
        $data .= '</sitemapindex>';
        $response->setContent($data);
        return $response;
    }

    public function pagesAction() {

        $response = new \Phalcon\Http\Response();
        $expireDate = new \DateTime();
        $expireDate->modify('+1 day');
        $response->setExpires($expireDate);
        $response->setHeader('Content-Type', "application/xml; charset=UTF-8");
        $sitemap = new \DOMDocument("1.0", "UTF-8");
        $urlset = $sitemap->createElement('urlset');
        $urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $urlset->setAttribute('xmlns:xsi', 'https://www.w3.org/2001/XMLSchema-instance');
        $urlset->setAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
        $modifiedAt = new \DateTime();
        $modifiedAt->setTimezone(new \DateTimeZone('UTC'));

        $links = array(
            array('u' => '/', 'p' => '1.0'),
            array('u' => '/about.html'),
            array('u' => '/privacy-policy.html'),
            array('u' => '/cara-kerja.html'),
            array('u' => '/term-of-use.html'),
        );

        foreach ($links as $link) {
            $url = $sitemap->createElement('url');
            $href = $this->domain . $link['u'];
            $url->appendChild($sitemap->createElement('loc', $href));
            if (!isset($link['p'])) {
                $url->appendChild($sitemap->createElement('changefreq', 'weekly')); //Hourly, daily, weekly etc.
            }
            if (isset($link['u']) && isset($link['p'])) {
                $url->appendChild($sitemap->createElement('priority', $link['p']));
            }
            $urlset->appendChild($url);
        }
        $sitemap->appendChild($urlset);
        $response->setContent($sitemap->saveXML());
        return $response;
    }

    public function categoriesAction() {
        $response = new \Phalcon\Http\Response();
        $expireDate = new \DateTime();
        $expireDate->modify('+1 day');
        $response->setExpires($expireDate);
        $response->setHeader('Content-Type', "application/xml; charset=UTF-8");
        $sitemap = new \DOMDocument("1.0", "UTF-8");
        $urlset = $sitemap->createElement('urlset');
        $urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $urlset->setAttribute('xmlns:xsi', 'https://www.w3.org/2001/XMLSchema-instance');
        $urlset->setAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
        $modifiedAt = new \DateTime();
        $modifiedAt->setTimezone(new \DateTimeZone('UTC'));

        $categories = \Dkm\Models\TblCategory::find(array("order" => "cid asc"));
        foreach ($categories as $cat) {
            $links[] = '/categories/view/' . $cat->cid . '/' . $cat->slug;
        }

        foreach ($links as $link) {
            $url = $sitemap->createElement('url');
            $href = $this->domain . $link;
            $url->appendChild($sitemap->createElement('loc', $href));
            $url->appendChild($sitemap->createElement('changefreq', 'daily')); //Hourly, daily, weekly etc.
            $url->appendChild($sitemap->createElement('priority', '1'));     //1, 0.7, 0.5 ...
            $urlset->appendChild($url);
        }
        $sitemap->appendChild($urlset);
        $response->setContent($sitemap->saveXML());
        return $response;
    }

    public function majorsAction() {
        $response = new \Phalcon\Http\Response();
        $expireDate = new \DateTime();
        $expireDate->modify('+1 day');
        $response->setExpires($expireDate);
        $response->setHeader('Content-Type', "application/xml; charset=UTF-8");
        $sitemap = new \DOMDocument("1.0", "UTF-8");
        $urlset = $sitemap->createElement('urlset');
        $urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $urlset->setAttribute('xmlns:xsi', 'https://www.w3.org/2001/XMLSchema-instance');
        $urlset->setAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
        $modifiedAt = new \DateTime();
        $modifiedAt->setTimezone(new \DateTimeZone('UTC'));

        $categories = \Dkm\Models\TblMajors::find(array("order" => "name asc"));
        foreach ($categories as $cat) {
            $links[] = '/jurusan/' . $cat->id . '/' . $cat->slug;
        }

        foreach ($links as $link) {
            $url = $sitemap->createElement('url');
            $href = $this->domain . $link;
            $url->appendChild($sitemap->createElement('loc', $href));
            $url->appendChild($sitemap->createElement('changefreq', 'daily')); //Hourly, daily, weekly etc.
            $url->appendChild($sitemap->createElement('priority', '1'));     //1, 0.7, 0.5 ...
            $urlset->appendChild($url);
        }
        $sitemap->appendChild($urlset);
        $response->setContent($sitemap->saveXML());
        return $response;
    }
    
    public function blogAction() {
        $response = new \Phalcon\Http\Response();
        $expireDate = new \DateTime();
        $expireDate->modify('+1 day');
        $response->setExpires($expireDate);
        $response->setHeader('Content-Type', "application/xml; charset=UTF-8");
        $sitemap = new \DOMDocument("1.0", "UTF-8");
        $urlset = $sitemap->createElement('urlset');
        $urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $urlset->setAttribute('xmlns:xsi', 'https://www.w3.org/2001/XMLSchema-instance');
        $urlset->setAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
        $modifiedAt = new \DateTime();
        $modifiedAt->setTimezone(new \DateTimeZone('UTC'));

        $posts = $this->cache->remember("SITEMAP__BLOG", 60, function() {
            return $this->dbblog->query("SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = 'post' ORDER BY ID DESC")->fetchAll();
        });
        
        foreach ($posts as $post) {
            $links[] = '/blog/' . $post['post_name'];
        }

        foreach ($links as $link) {
            $url = $sitemap->createElement('url');
            $href = $this->domain . $link;
            $url->appendChild($sitemap->createElement('loc', $href));
            $url->appendChild($sitemap->createElement('changefreq', 'daily')); //Hourly, daily, weekly etc.
            $url->appendChild($sitemap->createElement('priority', '1'));     //1, 0.7, 0.5 ...
            $urlset->appendChild($url);
        }
        $sitemap->appendChild($urlset);
        $response->setContent($sitemap->saveXML());
        return $response;
    }

    public function authorsAction() {
        $response = new \Phalcon\Http\Response();
        $expireDate = new \DateTime();
        $expireDate->modify('+1 day');
        $response->setExpires($expireDate);
        $response->setHeader('Content-Type', "application/xml; charset=UTF-8");
        $sitemap = new \DOMDocument("1.0", "UTF-8");
        $urlset = $sitemap->createElement('urlset');
        $urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $urlset->setAttribute('xmlns:xsi', 'https://www.w3.org/2001/XMLSchema-instance');
        $urlset->setAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
        $modifiedAt = new \DateTime();
        $modifiedAt->setTimezone(new \DateTimeZone('UTC'));

        $authors = \Dkm\Models\TblAuthor::find();
        foreach ($authors as $a) {
            $links[] = '/authors/detail/' . $a->author_id . '/' . $a->author_slug; //fixslugauthor
        }

        foreach ($links as $link) {
            $url = $sitemap->createElement('url');
            $href = $this->domain . $link;
            $url->appendChild($sitemap->createElement('loc', $href));
            $url->appendChild($sitemap->createElement('changefreq', 'daily')); //Hourly, daily, weekly etc.
            $url->appendChild($sitemap->createElement('priority', '1'));     //1, 0.7, 0.5 ...
            $urlset->appendChild($url);
        }
        $sitemap->appendChild($urlset);
        $response->setContent($sitemap->saveXML());
        return $response;
    }

    public function booksAction() {
        $response = new \Phalcon\Http\Response();
        $expireDate = new \DateTime();
        $expireDate->modify('+1 day');
        $response->setExpires($expireDate);
        $response->setHeader('Content-Type', "application/xml; charset=UTF-8");
        $sitemap = new \DOMDocument("1.0", "UTF-8");
        $urlset = $sitemap->createElement('urlset');
        $urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $urlset->setAttribute('xmlns:xsi', 'https://www.w3.org/2001/XMLSchema-instance');
        $urlset->setAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
        $modifiedAt = new \DateTime();
        $modifiedAt->setTimezone(new \DateTimeZone('UTC'));

        $books = \Dkm\Models\TblBooks::find(array("order" => "id asc"));
        foreach ($books as $book) {
            $links[] = '/books/view/' . $book->id . '/' . $book->slug;
        }

        foreach ($links as $link) {
            $url = $sitemap->createElement('url');
            $href = $this->domain . $link;
            $url->appendChild($sitemap->createElement('loc', $href));
            $url->appendChild($sitemap->createElement('changefreq', 'daily')); //Hourly, daily, weekly etc.
            $url->appendChild($sitemap->createElement('priority', '1'));     //1, 0.7, 0.5 ...
            $urlset->appendChild($url);
        }
        $sitemap->appendChild($urlset);
        $response->setContent($sitemap->saveXML());
        return $response;
    }

    public function requestAction() {
        $response = new \Phalcon\Http\Response();
        $expireDate = new \DateTime();
        $expireDate->modify('+1 day');
        $response->setExpires($expireDate);
        $response->setHeader('Content-Type', "application/xml; charset=UTF-8");
        $sitemap = new \DOMDocument("1.0", "UTF-8");
        $urlset = $sitemap->createElement('urlset');
        $urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $urlset->setAttribute('xmlns:xsi', 'https://www.w3.org/2001/XMLSchema-instance');
        $urlset->setAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
        $modifiedAt = new \DateTime();
        $modifiedAt->setTimezone(new \DateTimeZone('UTC'));

        $services = \Carijasa\Models\Services::find(array("order" => "id asc"));
        foreach ($services as $s) {
            $links[] = "/search/init/" . $s->id . "/" . $this->Util->SeoUrl($s->name);
        }

        foreach ($links as $link) {
            $url = $sitemap->createElement('url');
            $href = $this->domain . $link;
            $url->appendChild($sitemap->createElement('loc', $href));
            $url->appendChild($sitemap->createElement('changefreq', 'daily')); //Hourly, daily, weekly etc.
            $url->appendChild($sitemap->createElement('priority', '1'));     //1, 0.7, 0.5 ...
            $urlset->appendChild($url);
        }
        $sitemap->appendChild($urlset);
        $response->setContent($sitemap->saveXML());
        return $response;
    }

    public function propinsiAction($slug) {
        $response = new \Phalcon\Http\Response();
        $expireDate = new \DateTime();
        $expireDate->modify('+1 day');
        $response->setExpires($expireDate);
        $response->setHeader('Content-Type', "application/xml; charset=UTF-8");
        $sitemap = new \DOMDocument("1.0", "UTF-8");
        $urlset = $sitemap->createElement('urlset');
        $urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $urlset->setAttribute('xmlns:xsi', 'https://www.w3.org/2001/XMLSchema-instance');
        $urlset->setAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
        $modifiedAt = new \DateTime();
        $modifiedAt->setTimezone(new \DateTimeZone('UTC'));

        $services = \Carijasa\Models\Services::find(array("order" => "id asc"));
        $propinsi = \Carijasa\Models\Provinsi::find(array("order" => "id asc"));
        foreach ($services as $s) {
            $links[] = "/service/" . $s->slug;
            foreach ($propinsi as $p) {
                $links[] = "/service/" . $s->slug . "/" . $p->slug;
            }
        }

        foreach ($links as $link) {
            $url = $sitemap->createElement('url');
            $href = $this->domain . $link;
            $url->appendChild($sitemap->createElement('loc', $href));
            $url->appendChild($sitemap->createElement('changefreq', 'daily')); //Hourly, daily, weekly etc.
            $url->appendChild($sitemap->createElement('priority', '1'));     //1, 0.7, 0.5 ...
            $urlset->appendChild($url);
        }
        $sitemap->appendChild($urlset);
        $response->setContent($sitemap->saveXML());
        return $response;
    }

    public function servicesAction() {
        $response = new \Phalcon\Http\Response();
        $expireDate = new \DateTime();
        $expireDate->modify('+1 day');
        $response->setExpires($expireDate);
        $response->setHeader('Content-Type', "application/xml; charset=UTF-8");
        $sitemap = new \DOMDocument("1.0", "UTF-8");
        $urlset = $sitemap->createElement('urlset');
        $urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $urlset->setAttribute('xmlns:xsi', 'https://www.w3.org/2001/XMLSchema-instance');
        $urlset->setAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
        $modifiedAt = new \DateTime();
        $modifiedAt->setTimezone(new \DateTimeZone('UTC'));

        $services = \Carijasa\Models\Services::find(array("order" => "id asc"));
        foreach ($services as $s) {
            $links[] = "/service/" . $s->slug;
            $links[] = "/sitemap/service/" . $s->slug . "/provinsi.xml";
        }

        foreach ($links as $link) {
            $url = $sitemap->createElement('url');
            $href = $this->domain . $link;
            $url->appendChild($sitemap->createElement('loc', $href));
            $url->appendChild($sitemap->createElement('changefreq', 'daily')); //Hourly, daily, weekly etc.
            $url->appendChild($sitemap->createElement('priority', '1'));     //1, 0.7, 0.5 ...
            $urlset->appendChild($url);
        }
        $sitemap->appendChild($urlset);
        $response->setContent($sitemap->saveXML());
        return $response;
    }

    public function serviceAction($slug, $prop = null, $kota = null) {
        $response = new \Phalcon\Http\Response();
        $expireDate = new \DateTime();
        $expireDate->modify('+1 day');
        $response->setExpires($expireDate);
        $response->setHeader('Content-Type', "application/xml; charset=UTF-8");
        $sitemap = new \DOMDocument("1.0", "UTF-8");
        $urlset = $sitemap->createElement('urlset');
        $urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $urlset->setAttribute('xmlns:xsi', 'https://www.w3.org/2001/XMLSchema-instance');
        $urlset->setAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
        $modifiedAt = new \DateTime();
        $modifiedAt->setTimezone(new \DateTimeZone('UTC'));

        $service = \Carijasa\Models\Services::findFirstByslug($slug);
        $links[] = "/service/" . $service->slug;
        if ($prop == 'provinsi' && $kota == '') {
            $propinsi = \Carijasa\Models\Provinsi::find();
            foreach ($propinsi as $p) {
                $links[] = "/service/" . $service->slug . "/" . $p->slug;
                $links[] = "/sitemap/service/" . $service->slug . "/" . $p->slug . "/kota.xml";
            }
        }
        if ($kota != '') {
            $propinsi = \Carijasa\Models\Provinsi::findFirstByslug($prop);
            $links[] = "/service/" . $service->slug . "/" . $propinsi->slug;
            $kota = \Carijasa\Models\Kabupaten::find();
            foreach ($kota as $k) {
                $links[] = "/service/" . $service->slug . "/" . $propinsi->slug . "/" . $k->slug;
            }
        }

        foreach ($links as $link) {
            $url = $sitemap->createElement('url');
            $href = $this->domain . $link;
            $url->appendChild($sitemap->createElement('loc', $href));
            $url->appendChild($sitemap->createElement('changefreq', 'daily')); //Hourly, daily, weekly etc.
            $url->appendChild($sitemap->createElement('priority', '1'));     //1, 0.7, 0.5 ...
            $urlset->appendChild($url);
        }
        $sitemap->appendChild($urlset);
        $response->setContent($sitemap->saveXML());
        return $response;
    }

    public function providerAction($page = null) {

        $response = new \Phalcon\Http\Response();
        $expireDate = new \DateTime();
        $expireDate->modify('+1 day');
        $response->setExpires($expireDate);
        $response->setHeader('Content-Type', "application/xml; charset=UTF-8");
        $sitemap = new \DOMDocument("1.0", "UTF-8");
        $urlset = $sitemap->createElement('urlset');
        $urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $urlset->setAttribute('xmlns:xsi', 'https://www.w3.org/2001/XMLSchema-instance');
        $urlset->setAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
        $modifiedAt = new \DateTime();
        $modifiedAt->setTimezone(new \DateTimeZone('UTC'));

        $npp = 50;
        if ($page != '') {
            $start = $npp * (int) $page;
            $uss = $this->modelsManager->executeQuery("SELECT u.name as uname,u.id as uid, s.name as service_name "
                    . "FROM Carijasa\Models\Users u JOIN Carijasa\Models\UserServices us ON u.id=us.user_id "
                    . "JOIN Carijasa\Models\Services s ON s.id=us.service_id "
                    . "WHERE u.profilesId='4' LIMIT " . $start . " OFFSET " . $npp);
            foreach ($uss as $s) {
                $links[] = "/profile/" . $s->uid . "/" . $this->Util->SeoUrl($s->uname);
            }
        } else {
            $uss = $this->modelsManager->executeQuery("SELECT u.name as uname,u.id as uid, s.name as service_name "
                    . "FROM Carijasa\Models\Users u JOIN Carijasa\Models\UserServices us ON u.id=us.user_id "
                    . "JOIN Carijasa\Models\Services s ON s.id=us.service_id "
                    . "WHERE u.profilesId='4'");
            $pages = ceil(count($uss) / $npp);
            for ($i = 1; $i <= $pages; $i++) {
                $links[] = "/sitemap/provider/" . $i . ".xml";
            }
        }

        foreach ($links as $link) {
            $url = $sitemap->createElement('url');
            $href = $this->domain . $link;
            $url->appendChild($sitemap->createElement('loc', $href));
            $url->appendChild($sitemap->createElement('changefreq', 'daily')); //Hourly, daily, weekly etc.
            $url->appendChild($sitemap->createElement('priority', '1'));     //1, 0.7, 0.5 ...
            $urlset->appendChild($url);
        }
        $sitemap->appendChild($urlset);
        $response->setContent($sitemap->saveXML());
        return $response;
    }

    public function kotaAction() {
        $response = new \Phalcon\Http\Response();
        $expireDate = new \DateTime();
        $expireDate->modify('+1 day');
        $response->setExpires($expireDate);
        $response->setHeader('Content-Type', "application/xml; charset=UTF-8");
        $sitemap = new \DOMDocument("1.0", "UTF-8");
        $urlset = $sitemap->createElement('urlset');
        $urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $urlset->setAttribute('xmlns:xsi', 'https://www.w3.org/2001/XMLSchema-instance');
        $urlset->setAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
        $modifiedAt = new \DateTime();
        $modifiedAt->setTimezone(new \DateTimeZone('UTC'));

        $services = \Carijasa\Models\Services::find(array("order" => "id asc"));
        foreach ($services as $s) {
            $links[] = "/service/" . $s->slug;
        }

        foreach ($links as $link) {
            $url = $sitemap->createElement('url');
            $href = $this->domain . $link;
            $url->appendChild($sitemap->createElement('loc', $href));
            $url->appendChild($sitemap->createElement('changefreq', 'daily')); //Hourly, daily, weekly etc.
            $url->appendChild($sitemap->createElement('priority', '1'));     //1, 0.7, 0.5 ...
            $urlset->appendChild($url);
        }
        $sitemap->appendChild($urlset);
        $response->setContent($sitemap->saveXML());
        return $response;
    }

    public function projectsAction() {

        $response = new \Phalcon\Http\Response();
        $expireDate = new \DateTime();
        $expireDate->modify('+1 day');
        $response->setExpires($expireDate);
        $response->setHeader('Content-Type', "application/xml; charset=UTF-8");
        $sitemap = new \DOMDocument("1.0", "UTF-8");
        $urlset = $sitemap->createElement('urlset');
        $urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $urlset->setAttribute('xmlns:xsi', 'https://www.w3.org/2001/XMLSchema-instance');
        $urlset->setAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
        $modifiedAt = new \DateTime();
        $modifiedAt->setTimezone(new \DateTimeZone('UTC'));

        $projects = \Carijasa\Models\Projects::find(array("order" => "id asc"));
        foreach ($projects as $p) {
            $links[] = "/profile/projects/" . $p->user_id . "/" . $this->Util->SeoUrl($p->users->name);
            //http://www.carijasa.co.id/profile/projects/1987/banten-tangerang-do-creative-house.html
        }

        foreach ($links as $link) {
            $url = $sitemap->createElement('url');
            $href = $this->domain . $link;
            $url->appendChild($sitemap->createElement('loc', $href));
            $url->appendChild($sitemap->createElement('changefreq', 'daily')); //Hourly, daily, weekly etc.
            $url->appendChild($sitemap->createElement('priority', '1'));     //1, 0.7, 0.5 ...
            $urlset->appendChild($url);
        }
        $sitemap->appendChild($urlset);
        $response->setContent($sitemap->saveXML());
        return $response;
    }

    public function referencesAction() {
        $response = new \Phalcon\Http\Response();
        $expireDate = new \DateTime();
        $expireDate->modify('+1 day');
        $response->setExpires($expireDate);
        $response->setHeader('Content-Type', "application/xml; charset=UTF-8");
        $sitemap = new \DOMDocument("1.0", "UTF-8");
        $urlset = $sitemap->createElement('urlset');
        $urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $urlset->setAttribute('xmlns:xsi', 'https://www.w3.org/2001/XMLSchema-instance');
        $urlset->setAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
        $modifiedAt = new \DateTime();
        $modifiedAt->setTimezone(new \DateTimeZone('UTC'));

        $references = \Carijasa\Models\UserReferences::find(array("order" => "id asc"));
        foreach ($references as $r) {
            $links[] = "/profile/references/" . $r->user_id . "/" . $this->Util->SeoUrl($r->users->name);
        }

        foreach ($links as $link) {
            $url = $sitemap->createElement('url');
            $href = $this->domain . $link;
            $url->appendChild($sitemap->createElement('loc', $href));
            $url->appendChild($sitemap->createElement('changefreq', 'daily')); //Hourly, daily, weekly etc.
            $url->appendChild($sitemap->createElement('priority', '1'));     //1, 0.7, 0.5 ...
            $urlset->appendChild($url);
        }
        $sitemap->appendChild($urlset);
        $response->setContent($sitemap->saveXML());
        return $response;
    }

    public function reviewsAction() {
        $response = new \Phalcon\Http\Response();
        $expireDate = new \DateTime();
        $expireDate->modify('+1 day');
        $response->setExpires($expireDate);
        $response->setHeader('Content-Type', "application/xml; charset=UTF-8");
        $sitemap = new \DOMDocument("1.0", "UTF-8");
        $urlset = $sitemap->createElement('urlset');
        $urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $urlset->setAttribute('xmlns:xsi', 'https://www.w3.org/2001/XMLSchema-instance');
        $urlset->setAttribute('xsi:schemaLocation', 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd');
        $modifiedAt = new \DateTime();
        $modifiedAt->setTimezone(new \DateTimeZone('UTC'));

        $reviews = \Carijasa\Models\UserReviews::find(array("order" => "id asc"));
        foreach ($reviews as $r) {
            $links[] = "/profile/reviews/" . $r->user_id . "/" . $this->Util->SeoUrl($r->users->name);
        }

        foreach ($links as $link) {
            $url = $sitemap->createElement('url');
            $href = $this->domain . $link;
            $url->appendChild($sitemap->createElement('loc', $href));
            $url->appendChild($sitemap->createElement('changefreq', 'daily')); //Hourly, daily, weekly etc.
            $url->appendChild($sitemap->createElement('priority', '1'));     //1, 0.7, 0.5 ...
            $urlset->appendChild($url);
        }
        $sitemap->appendChild($urlset);
        $response->setContent($sitemap->saveXML());
        return $response;
    }

}
