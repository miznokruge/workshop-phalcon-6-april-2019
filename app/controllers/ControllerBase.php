<?php

namespace Dkm\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use Dkm\Models\TblCategory;
use Dkm\Forms\LoginForm;
use Dkm\Forms\SignUpForm;
use Dkm\Models\Category;
use Dkm\Models\Mosque;
use Dkm\Models\Kota;

/**
 * ControllerBase
 * This is the base controller for all controllers in the application
 *
 * @property \Dkm\Auth\Auth auth
 */
class ControllerBase extends Controller {

    public $title;

    public function initialize() {
        // if (!$this->auth->getIdentity()) {
        //     return $this->response->redirect('/');
        // }
        $this->view->setTemplateBefore('public');
        $this->view->form = new LoginForm();
        $this->view->formDaftar = new SignUpForm();
        $kota = $this->cache->remember("FOOTER_KOTA", 60, function() {
            return Kota::find(['order' => 'name ASC' ,'limit' => 6]);
        });
        $categories = $this->cache->remember("FOOTER_MOSQUE_CATEGORY", 60, function() {
            return Category::find();
        });
        $mosque = $this->cache->remember("FOOTER_LAST_6_MOSQUES", 60, function() {
            return Mosque::find(['limit' => 6]);
        });
        $this->view->kota_on_footer = $kota;
        $this->view->categories_on_header = $categories;
        $this->view->mosque_footer = $mosque;
    }

    public function debug($var, $stop = false) {
        echo "<pre>";
        print_r($var);
        echo "</pre>";

        if ($stop) {
            die;
        }
    }

    /**
     * Execute before the router so we can determine if this is a private controller, and must be authenticated, or a
     * public controller that is open to all.
     *
     * @param Dispatcher $dispatcher
     * @return boolean
     */
}
