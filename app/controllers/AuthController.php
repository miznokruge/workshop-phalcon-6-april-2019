<?php

namespace Dkm\Controllers;

class AuthController extends ControllerBase {

    public function initialize() {
        parent::initialize();
    }
    public function indexAction() {

        $dir = str_replace("\\", "/", APP_PATH) . '/library/';
        $auth = $dir . "hybridauth/Hybrid/Auth.php";
        $end = $dir . "hybridauth/Hybrid/Endpoint.php";
        include_once $auth;
        include_once $end;
        $h = new \Hybrid_Endpoint();
        $h->process();
    }

    public function socialAction() {

        $login = $this->hybridauth->authenticate($this->request->getQuery('hauth.start', 'striptags'));
        $profile = $login->getUserProfile();
        $this->debug($profile);
    }
    
    public function deauthAction(){
        die('deauth');
    }

}
