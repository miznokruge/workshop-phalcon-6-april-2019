<?php

namespace Dkm\Controllers;

use Dkm\Util\Util;
use Dkm\Forms\LoginForm;
use Dkm\Forms\SignUpForm;
use Dkm\Models\Category;
use Dkm\Models\Posts;
use Dkm\Models\Mosque;
use Phalcon\Mvc\Url;
use Phalcon\Paginator\Adapter\Model as Paginator;

/**
 * Display the default index page.
 */
class IndexController extends ControllerBase {

    // public function initialize() {
    //     parent::initialize();
    //     $this->view->setVar('logged_in', is_array($this->auth->getIdentity()));
    // }

    public function hAction() {
        echo $this->security->hash('123');
        echo bcrypt();
    }

    public function ccAction() {
        $this->cache->flush();
        die("CACHE_FLUSH");
    }

    public function track_invitationAction($uid, $email = null) {
        $user = \Dkm\Models\TblUsers::findFirstByid($uid);
        if ($user) {
            $expire = time() + 86400 * 8;
            $this->cookies->set('invite_to_join_uid', $uid, $expire);
            return $this->response->redirect('/session/signup');
        } else {
            return $this->response->redirect('/session/signup');
        }
    }

    public function indexAction() {
        $this->view->setTemplateBefore('landing');
        $this->tag->setTitle('Direktori masjid dan platform manajemen masjid terlengkap di indonesia | DKM.or.id');
        $posts = Posts::find(['order' => 'created desc', 'limit' => 3]);
        $mosque = $this->cache->remember("INDEX_MOSQUE", 60, function() {
            return Mosque::find(['order' => 'created desc', 'limit' => 5]);
        });

        $this->view->mosque = $mosque;
        $this->view->kota = \Dkm\Models\Kota::find(['order' => 'urutan asc']);
        //$this->debug($kota->toArray());die;
        $this->view->posts = $posts;
        $categories = $this->cache->remember("INDEX_MOSQUE_CATEGORY", 60, function() {
            return Category::find();
        });
        $this->view->categories = $categories;
    }

    public function contactAction() {
        $this->view->setTemplateBefore('listing');
    }

    public function aboutAction() {
        $this->view->setTemplateBefore('listing');
    }

    public function cara_kerjaAction() {
        $this->view->setTemplateBefore('listing');
    }

    public function term_of_useAction() {
        $this->view->setTemplateBefore('public');
    }

    public function landing_signupAction(){
        $this->view->setTemplateBefore('listing');
    }

    public function categoryAction($id) {
        $category = TblCategory::findFirstBycid($id);

        $books = TblBooks::find(array("cat_id=" . $id));

        $this->view->category = $category;

        $currentPage = (int) $_GET['page'];
        $paginator = new Paginator(
                [
            'data' => $books,
            'limit' => 9,
            'page' => $currentPage,
                ]
        );
        $this->view->page = $paginator->getPaginate();
    }

    public function privacy_policyAction() {
        $this->view->setTemplateBefore('public');
    }

    public function signupAction() {
        if ($this->request->isPost()) {
            $users = new TblUsers();
            $users->assign(array(
                'user_type' => 'Normal',
                'name' => $this->request->getPost('name'),
                'email' => $this->request->getPost('email'),
                'password' => $this->request->getPost('pass'),
                'phone' => $this->request->getPost('phone'),
            ));
            if (!$users->save()) {
                // $this->flashSession->error($users->getMessages());
                $this->debug($users->getMessages());
                die;
            }
            $this->flashSession->success('Anda Berhasil Mendaftar');
            return $this->response->redirect('/');
        }
    }

    public function testimoniAction() {
        $this->view->setTemplateBefore('landing');
        $this->tag->setTitle("Dashboard | Admin");
    }

    public function loginnAction(){
        if(is_array($this->auth->getIdentity())):
            return $this->response->redirect('/dashboard');
        endif;
        $this->view->setTemplateBefore('no-layout');
    }

}
