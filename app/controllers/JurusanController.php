<?php

namespace Dkm\Controllers;

use Dkm\Util\Util;
use Dkm\Models\TblBooks;
use Dkm\Models\TblCategory;
use Dkm\Models\TblAuthor;
use Dkm\Models\TblMajors;
use Phalcon\Mvc\Url;
use Phalcon\Paginator\Adapter\Model as Paginator;

class JurusanController extends ControllerBase {

    public function initialize() {
        parent::initialize();
        $this->view->setTemplateBefore('public');
        $this->view->setVar('logged_in', is_array($this->auth->getIdentity()));
    }

    
    public function showAction() {
        $cs = TblMajors::find();
        $s='Skripsi, tesis dan disertasi jurusan ';
        foreach ($cs as $c) {
            $s.=ucwords(strtolower($c->name)).',';
        }
        echo $s;
        die("UPDATE_SLUG_DONE_MAJOR");
    }
    
    public function fixslugAction() {
        $cs = TblMajors::find();
        foreach ($cs as $c) {
            $c->name = strtolower($c->name);
            $c->slug = $this->Util->SeoUrl('jurusan ' . $c->name,true);
            $c->description = 'Skripsi, tesis dan disertasi jurusan ' . $c->name;
            $c->save();
        }
        die("UPDATE_SLUG_DONE_MAJOR");
    }

    public function fixslugauthorAction() {
        $cs = TblAuthor::find();
        foreach ($cs as $c) {
            $c->author_slug = $this->Util->SeoUrl($c->author_name);
            $c->save();
        }
        die("UPDATE_SLUG_DONE");
    }

    public function viewAction($id) {
        $jur = TblMajors::findFirstByid($id);
        $category_list = TblCategory::find();
        $author = TblAuthor::find();
        $books = TblBooks::find(['order'=>'id desc']);

        $currentPage = $this->request->getQuery('page', 'int', 1);
        $paginator = new Paginator(
                [
            'data' => $books,
            'limit' => 12,
            'page' => $currentPage,
                ]
        );
        $buku = [];
        foreach ($books as $book) {
            $buku[] = $book->book_title;
        }
        $this->view->jur = $jur;
        $this->view->category_list = $category_list;
        $this->view->author = $author;

        $this->view->meta = array('d' => $jur->name . ' Semua skripsi,tesis, dan disertasi di Jurusan ini ' . implode('', $buku), 't' => 'Skripsi, tesis dan disertasi jurusan ' . $jur->name . ' | Skripsi.co.id');
        $this->view->page = $paginator->getPaginate();
        $this->assets->addCss('css/category/index.css');
    }

    public function indexAction() {
        $majors = $this->cache->remember("MAJORS", 60, function() {
            return TblMajors::find(['order' => 'name asc']);
        });
        $s='Koleksi skripsi terlengkap semua jurusan: ';
        foreach ($majors as $c) {
            $s.=ucwords(strtolower($c->name)).',';
        }
        $this->view->majors=$majors;
        $this->view->meta = array('d' =>$s, 
            't' => 'Koleksi skripsi terlengkap dari berbagai jurusan | Skripsi.co.id');
        
    }

}
