<?php

namespace Dkm\Controllers;

use Dkm\Util\Util;
use Phalcon\Mvc\Url;
use Phalcon\Paginator\Adapter\Model as Paginator;

class BlogsController extends ControllerBase {

    public function initialize() {
        parent::initialize();
        $this->view->setVar('logged_in', is_array($this->auth->getIdentity()));
    }

    public function fsAction() {
        $posts = \Dkm\Models\Posts::find();
        foreach ($posts as $c) {
            $sloug = $this->Util->SeoUrl($c->title);
            $c->slug = $sloug;
            if (!$c->save()) {
                print_r($c->getMessages());
                die;
            }
        }
        die("UPDATE_SLUG_DONE");
    }

    public function viewAction($id) {
        $this->view->setTemplateBefore('listing');
        $post = \Dkm\Models\Posts::findFirstByid($id);
        $this->tag->setTitle($post->title . ' | Blog - DKM.or.id');
        $this->view->post = $post;
    }

    public function indexAction() {
        $this->view->setTemplateBefore('landing');
        $this->tag->setTitle('Berita - DKM.or.id');
        $p = $this->request->getQuery("page", "int", 1);
        $posts = \Dkm\Models\Posts::find();
        $paginator = new Paginator(array(
            "data" => $posts,
            "limit" => 3,
            "page" => $p
        ));
        $this->view->page = $paginator->getPaginate();
    }

}
