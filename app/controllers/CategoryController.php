<?php

namespace Dkm\Controllers;

use Dkm\Util\Util;
use Dkm\Models\TblBooks;
use Dkm\Models\TblCategory;
use Dkm\Models\TblAuthor;
use Phalcon\Mvc\Url;
use Phalcon\Paginator\Adapter\Model as Paginator;

class CategoryController extends ControllerBase {

    public function initialize() {
        parent::initialize();
        $this->view->setTemplateBefore('public');
        $this->view->setVar('logged_in', is_array($this->auth->getIdentity()));
    }

    public function fsAction() {
        $cs = \Dkm\Models\Category::find();
        foreach ($cs as $c) {
            $sloug = $this->Util->SeoUrl($c->name);
            $c->slug = $sloug;
            if (!$c->save()) {
                print_r($c->getMessages());
                die;
            }
        }
        die("UPDATE_SLUG_DONE");
    }

    public function fixslugauthorAction() {
        $cs = TblAuthor::find();
        foreach ($cs as $c) {
            $c->author_slug = $this->Util->SeoUrl($c->author_name);
            $c->save();
        }
        die("UPDATE_SLUG_DONE");
    }

    public function viewAction($id) {

        $this->view->setTemplateBefore('listing');
        $category = \Dkm\Models\Category::findFirstByid($id);
        $this->tag->setTitle('Kategori Masjid ' . $category->name . ' | DKM.or.id');
        $mosques = \Dkm\Models\Mosque::find(array("category_id=" . $id, 'order' => 'id desc'));
        $currentPage = $this->request->getQuery('page', 'int', 1);
        $paginator = new Paginator(['data' => $mosques,'limit' => 5,'page' => $currentPage]);
        $this->view->page = $paginator->getPaginate();
        $this->view->category = $category;
        $this->view->meta = array('d' => $category->category_name . ' Semua Judul Buku di Kategori ini ' . implode('', $buku), 't' => $category->category_name . ' | Skripsi.co.id');

        $this->assets->addCss('css/category/index.css');
    }

    public function indexAction() {
        $this->view->meta = array('d' => '', 't' => ' ALL Categories | Skripsi.co.id');
    }

}
