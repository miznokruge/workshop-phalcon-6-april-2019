<?php

namespace Dkm\Controllers;

use Dkm\Util\Util;
use Dkm\Models\TblBooks;
use Dkm\Models\TblCategory;
use Dkm\Models\TblRating;
use Dkm\Models\TblAuthor;
use Dkm\Models\TblComments;
use Phalcon\Mvc\Url;
use Phalcon\Paginator\Adapter\Model as Paginator;

class DkmController extends ControllerBase {

    public function initialize() {
        parent::initialize();
        $this->view->setVar('logged_in', is_array($this->auth->getIdentity()));
    }

    public function searchAction() {

        $params = '1';
        if (isset($_GET['kota'])) {
            $params .= " AND city_id='" . $this->request->getQuery('kota') . "'";
        }

        $p['order'] = 'id desc';

        $this->view->setTemplateBefore('listing');
        $this->tag->setTitle('Search Masjid - DKM.or.id');
        $mosques = \Dkm\Models\Mosque::find([$params, $p]);
        $currentPage = $this->request->getQuery('page', 'int', 1);
        $paginator = new Paginator(['data' => $mosques, 'limit' => 5, 'page' => $currentPage]);
        $this->view->page = $paginator->getPaginate();
        $this->view->category = $category;
        $this->view->meta = array('d' => 'Semua DKM ', 't' => 'Search DKM - DKM.or.id');

        $this->assets->addCss('css/category/index.css');
    }

    public function pengurusAction() {
        $this->view->setTemplateBefore('pengurus');
    }

    public function fsAction() {
        $cs = \Dkm\Models\Mosque::find();
        foreach ($cs as $c) {
            $c->slug = $this->Util->SeoUrl($c->name . ' ' . $c->location);
            if (!$c->save()) {
                print_r($c->getMessages());
                die;
            }
        }
        die("UPDATE_SLUG_DONE");
    }

    public function addAction() {
        $this->assets->addJs('/ckeditor/ckeditor.js');
    }

    public function indexAction() {
        $this->view->setTemplateBefore('public');
        $category_list = TblCategory::find();
        $author = TblAuthor::find();

        $params = [];
        $str = ' 1=1 ';
        if (isset($_GET['q']) && $_GET['q'] != '') {
            $str .= " AND book_title LIKE '%" . $this->request->getQuery('q', 'striptags') . "%'";
        }

        if (isset($_GET['category']) && $_GET['category'] != '') {
            $str .= " AND cat_id='" . $this->request->getQuery('category', 'int', 0) . "'";
        }

        if (isset($_GET['author']) && $_GET['author'] != '') {
            $params['TblBooks.aid'] = $this->request->getQuery('author', 'int', 0);
            $str .= " AND aid='" . $this->request->getQuery('author', 'int', 0) . "'";
        }
        if (isset($_GET['sort_by']) && $_GET['sort_by'] != '') {

            switch ($_GET['sort_by']) {
                case "1":
                    $params['order'] = "rate_avg DESC";
                    break;
                case "2":
                    $params['order'] = "book_views DESC";
                    break;
                default:
                    $params['order'] = "book_views DESC";
            }
        }

        $cond = [$str, $params];

        $mosque = TblBooks::find($cond);

        $this->view->category = $category;
        $this->view->category_list = $category_list;
        $this->view->author = $author;
        // $currentPage = (int) $_GET['page'];
        $currentPage = $this->request->getQuery('page', 'int', 1);
        $paginator = new Paginator(
                [
            'data' => $mosque,
            'limit' => 12,
            'page' => $currentPage,
                ]
        );

        $buku = [];
        foreach ($mosque as $mosque) {
            $buku[] = $mosque->book_title;
        }

        $this->view->meta = array('d' => ' Semua Judul Buku di Skripsi.co.id ' . implode('', $buku), 't' => ' ALL BOOKS | Skripsi.co.id');
        $this->view->page = $paginator->getPaginate();

        $this->assets->addCss('select2/css/select2.min.css');
        $this->assets->addCss('css/category/index.css');
        $this->assets->addJs('select2/js/select2.min.js');
        $this->assets->addJs('js/jquery.matchHeight.js');
        $this->assets->addJs('js/books/index100.js');
    }

    public function viewAction($id) {
        $this->view->setTemplateBefore('listing');
        $mosque = \Dkm\Models\Mosque::findFirstByid($id);
        $reviews = \Dkm\Models\Reviews::find(["mosque_id=" . $id, 'order' => 'created desc']);
        // $data_reviews = $this->query("select id, mosque_id, user_id, name, email, AVG(rating), comment, created FROM reviews ORDER BY created DESC")->fetchAll();
        $total = $this->db->query("select AVG(reviews.rating) as rate, mosque.id FROM reviews JOIN mosque ON mosque.id=reviews.mosque_id WHERE mosque.id='" . $id . "'")->fetchAll()[0];
        $mosque->total_rating = (int) $total['rate'];
        if ($this->request->isPost()) {
            $review = new \Dkm\Models\Reviews();
            $review->mosque_id = $id;
            $review->user_id = is_array($this->auth->getIdentity()) ? $this->auth->getIdentity()['id'] : 0;
            $review->name = is_array($this->auth->getIdentity()) ? $this->auth->getIdentity()['name'] : $this->request->getPost('yourname', 'striptags', '');
            $review->email = is_array($this->auth->getIdentity()) ? $this->auth->getIdentity()['email'] : $this->request->getPost('emailaddress', 'striptags', '');
            $review->comment = $this->request->getPost('review', 'striptags', '');
            $review->created = date("Y-m-d H:i:s");
            $review->rating = $this->request->getPost('rating', 'int', 3);
            if ($review->save()) {
                echo json_encode(array("success" => true));
            } else {
                echo json_encode(array("success" => false));
            }
            die;
        }
        $this->assets->addCss('/css/jssocials-theme-classic.css')
                ->addCss('/css/jssocials.css')
                ->addJs('/js/jssocials.min.js')
                ->addJs('/js/dkm-view.js');
        $this->tag->setTitle('Masjid ' . $mosque->name . ' ' . $mosque->location . ' - DKM.or.id');
        $mosque->mosque_views = $mosque->mosque_views + 1;
        $mosque->save();
        $this->view->mosque = $mosque;
        $this->view->reviews = $reviews;
        $meta = [
            't' => $mosque->name . ' ' . $mosque->location,
            'd' => strip_tags($mosque->description),
            'img' => ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http") . '://' . $_SERVER['HTTP_HOST'] . ($mosque->image == '' ? '/img/masjid/backgroundmasjid.jpg' : '/img/masjid/member/' . $mosque->image),
            'site' => ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http") . '://' . $_SERVER['HTTP_HOST'],
            'url' => ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http") . '://' . $_SERVER['HTTP_HOST'] . '/dkm/' . $mosque->id . '/' . $mosque->slug,
            'creator' => 'DKM.or.id Team'
        ];
        $this->view->meta = $meta;
    }

    public function searchxAction() {
        $this->view->setTemplateBefore('public');
        // $this->debug($this->request->getQuery());die;
        $cid = $this->request->getQuery('category');
        $aid = $this->request->getQuery('author');
        $pop = $this->request->getQuery('popular');

        $where = " status='1' ";

        if ($cid != null) {
            $where .= " AND cat_id ='" . $cid . "' ";
        }

        if ($aid != null) {
            $where .= " AND aid ='" . $aid . "' ";
        }

        if ($pop != null) {
            if ($pop == 1) {
                $order_by = " rate_avg DESC";
            } else {
                $order_by = " book_views DESC";
            }
        }
        $query = ($pop != '' ? 'SELECT * FROM tbl_books WHERE ' . $where . ' ORDER BY ' . $order_by . '' : 'SELECT * FROM tbl_books WHERE ' . $where . '');

        if ($query == '') {
            $this->flashSession->error("Data not found");
        }
        echo $query;
        die;
        $hasil = $this->db->query($query);
        $mosques = TblBooks::find([$hasil]);
        // $this->debug($mosque->toArray());die;

        $this->view->books = $mosques;
    }

}
