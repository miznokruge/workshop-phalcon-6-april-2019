<?php

namespace Dkm\Controllers;

use DiDom\Document;

/**
 * Display the "About" page.
 */
class ScrapeController extends ControllerBase {

    function indexAction() {
        $document = new Document('http://gunarga.com/akuntansi.php', true);
        $title = $document->find('.title');
        $contents = $document->find('.one-half');

        ///print_r($title);

        foreach ($title as $t) {
            echo $t->text(), "\n";
        }

        foreach ($contents as $c) {
            echo $c->text(), "\n";
        }

        //echo $content;

        /*
         * $posts = $document->find('.card-service');


          foreach ($posts as $post) {
          echo $post->text(), "\n";
          }
          die;
         * 
         */

        die;
    }

    function masjidAction() {
        $document = new Document('http://simas.kemenag.go.id/index.php/profil/masjid/?tipologi_id=2', true);
        $contents = $document->find('#the-list tr');

        $paging = $document->find('.paging');
        foreach($paging as $p){
            echo $p->html();
        }

        $masjids = $document->find('#the-list tr');
        $data = [];
        for ($i = 0; $i < count($masjids); $i++) {
            //echo $masjids[$i]->html();
            $koloms = $masjids[$i]->find('td');
            for ($j = 0; $j < count($koloms); $j++) {
                $data[$i][$j] = $koloms[$j]->html();
            }
        }

        echo '<pre>';
        print_r($data);
        echo '</pre>';
        die;
    }

}
