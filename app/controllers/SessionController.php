<?php

namespace Dkm\Controllers;

use Dkm\Forms\LoginForm;
use Dkm\Forms\SignUpForm;
use Dkm\Forms\ForgotPasswordForm;
use Dkm\Auth\Exception as AuthException;
use Dkm\Models\Users;
use Dkm\Models\ResetPasswords;

/**
 * Controller used handle non-authenticated session actions like login/logout, user signup, and forgotten passwords
 */
class SessionController extends ControllerBase {

    protected $hybridauth = null;
    public $hconfig;

    const REDIRECT_FAILURE = 'login';
    const REDIRECT_SUCCESS = 'dashboard';

    public function facebookAction() {
        try {
            $facebookUser = $this->facebook->api('/me');
        } catch (\FacebookApiException $e) {
            $this->flashSession->error("Could not fetch your facebook user.");
            return $this->response->redirect(self::REDIRECT_FAILURE);
        }
        $user = \User::findFirstByFacebookId($facebookId);
        if (!$user) {
            // Save user record in database
            $user = new \User();
            $user->facebook_id = $facebookUser['id'];
            $user->create();
            if ($user->getMessages()) {
                $error = new \Error();
                $error->content = "Error creating facebook user with id {$facebookUser['id']}";
                $this->flashSession->error('There was an error connecting your facebook user.');
                return $this->response->redirect(self::REDIRECT_FAILURE);
            }
        }
        // This is how I create a Session, do it your own way
        $this->component->user->createSession($user, [
            'facebook_id' => $facebookId,
            'facebook_logout_url' => $this->facebook->getLogoutUrl()
        ]);
        return $this->response->redirect(self::REDIRECT_SUCCESS);
    }

    public function onConstruct() {
        $this->hconfig = str_replace("\\", "/", APP_PATH) . '/library/hybridauth/config.php';
        require_once( str_replace("\\", "/", APP_PATH) . "/library/hybridauth/Hybrid/Auth.php" );
        // $this->hybridauth = new \Hybrid_Auth($config);
    }

    public function getHAuth() {
        if (null === $this->hybridauth)
            $this->hybridauth = new \Hybrid_Auth($this->hconfig);

        return $this->hybridauth;
    }

    public function authAction() {
        $dir = dirname(dirname(__FILE__)) . '/library/';
        require_once($dir . "hybridauth/Hybrid/Auth.php");
        require_once($dir . "hybridauth/Hybrid/Endpoint.php");
        $h = new \Hybrid_Endpoint();
        $h->process();
    }

    public function facebook_logoutAction() {
        $this->session->destroy();
        $this->facebook->destroySession();
        $this->facebook->setAccessToken('');
        return $this->response->redirect($this->facebook->getLogoutUrl(), true);
    }

    public function socialAction($type = null, $account_type = 'klien') {

        error_reporting(E_ALL);

        if ($type == 'Yahoo' || $type == 'Twitter') {
            return $this->response->redirect('/');
        }
        try {
            // create an instance for Hybridauth with the configuration file path as parameter
            // try to authenticate the user with twitter,
            // user will be redirected to Twitter for authentication,
            // if he already did, then Hybridauth will ignore this step and return an instance of the adapter
            $adapter = $this->gethAuth()->authenticate($type);
            echo 'here1';
            $go = $this->request->getQuery('redir');
            // get the user profile
            $profile = $adapter->getUserProfile();

            echo 'here2';
            if ($profile->email == '') {
                $this->flash->error('We have problem connecting your account with <strong>' . $type . '</strong> Please register using another option');
                $this->dispatcher->forward(
                        array(
                            'controller' => 'error',
                            'action' => 'showsme',
                        )
                );
            }            
            //cek lookup ke db pake email
            //jika ketemu, maka login, jika tidak, maka register
            $user_found = Users::findFirst(["email='" . $profile->email . "'"]);
            if (false === $user_found) {
                
                $pass=time();
                $user = new Users([
                    'name' => $profile->firstName.' '.$profile->lastName,
                    'username' => $profile->email,
                    'email' => $profile->email,
                    'phone' => $profile->phone,
                    'password' => $this->security->hash($pass),
                    'web_password' => $this->security->hash($pass)
                ]);
                if ($user->save()) {
                    $this->auth->authUserByEmail($profile->email);
                    $this->flashSession->success('Signup berhasil');
                    return $this->response->redirect('/session/thankyou');
                }else{
                    $this->debug($user->getMessages());die;                    
                }
            } else {
                $this->auth->authUserByEmail($user_found->email);
                return $this->response->redirect('/dashboard');
            }
        } catch (Exception $e) {

            var_dump($e);
            die;

            $msg = json_encode(array(
                'text' => "error login using " . $type . "\n"
                . "because:\n"
                . "Msg:" . $e->getMessage() . "\n"
                . "Code:" . $e->getCode()
            ));
            $this->Util->sendToSlack($msg);

            // Display the recived error,
            // to know more please refer to Exceptions handling section on the userguide
            switch ($e->getCode()) {
                case 0 : echo "Unspecified error.";
                    break;
                case 1 : echo "Hybriauth configuration error.";
                    break;
                case 2 : echo "Provider not properly configured.";
                    break;
                case 3 : echo "Unknown or disabled provider.";
                    break;
                case 4 : echo "Missing provider application credentials.";
                    break;
                case 5 : echo "Authentification failed. "
                    . "The user has canceled the authentication or the provider refused the connection.";
                    break;
                case 6 : echo "User profile request failed. Most likely the user is not connected "
                    . "to the provider and he should authenticate again.";
                    $login->logout();
                    break;
                case 7 : echo "User not connected to the provider.";
                    $login->logout();
                    break;
                case 8 : echo "Provider does not support this feature.";
                    break;
            }
            // well, basically your should not display this to the end user, just give him a hint and move on..
            echo "<br /><br /><b>Original error message:</b> " . $e->getMessage();
        }
    }

    /**
     * Default action. Set the public layout (layouts/public.volt)
     */
    public function initialize() {
        parent::initialize();
    }

    public function indexAction() {
        
    }

    /**
     * Allow a user to signup to the system
     */
    public function signupAction() {

        $this->view->setTemplateBefore('public');
        if ($this->request->isPost()) {
            $pass = $this->request->getPost('password', 'striptags');
            $user = new Users([
                'name' => $this->request->getPost('name', 'striptags'),
                'username' => $this->request->getPost('username', 'striptags'),
                'email' => $this->request->getPost('email', 'striptags'),
                'phone' => $this->request->getPost('phone', 'striptags'),
                'password' => $this->security->hash($pass),
                'web_password' => $this->security->hash($pass)
            ]);
            if ($user->save()) {
                $this->flashSession->success('Permintaan anda akan segera kami proses');
                return $this->response->redirect('/session/thankyou');
            }

            $this->flashSession->error('gagal mendaftar');
            return $this->response->redirect('/session/signup');
        }
        $form = new SignUpForm();
        $this->view->form = $form;
    }

    public function ajax_signupAction() {
        $resp = new \Phalcon\Http\Response();
        if ($this->request->isPost()) {
            $pass = $this->request->getPost('password', 'striptags');
            $user = new Users([
                'name' => $this->request->getPost('name', 'striptags'),
                'username' => $this->request->getPost('email', 'striptags'),
                'email' => $this->request->getPost('email', 'striptags'),
                'phone' => $this->request->getPost('phone', 'striptags'),
                'password' => $this->security->hash($pass),
                'web_password' => $this->security->hash($pass)
            ]);
            if (!$user->save()) {
                return $resp->setContent(json_encode(['success' => false, 'msg' => $user->getMessages()]));
            } else {
                $this->auth->authByUserId($user->id);
                return $resp->setContent(json_encode(['success' => true, 'next' => '/session/thankyou']));
            }
        }
    }

    public function thankyouAction() {
        $this->tag->setTitle('Pendaftaran Berhasil | Cek Email Anda Untuk Mengaktifkan Akun');
    }

    public function resend_activationAction() {
        if ($this->request->isPost()) {
            $email = $this->request->getPost('email', 'email');
            $user = Users::findFirstByemail($email);
            if (!$user) {
                $this->flashSession->error("Email tidak terdaftar");
                return $this->response->redirect($_SERVER['HTTP_REFERER']);
            }
            $emailConfirmation = new \Dkm\Models\EmailConfirmations();
            $emailConfirmation->usersId = $user->id;
            $emailConfirmation->confirmed = 'N';
            $emailConfirmation->createdAt = time();
            $emailConfirmation->code = preg_replace('/[^a-zA-Z0-9]/', '', base64_encode(openssl_random_pseudo_bytes(24)));

            if ($emailConfirmation->save()) {
                $this->mail->send([
                    $user->email => $user->name
                        ], "Please confirm your email", 'confirmation', [
                    'confirmUrl' => '/confirm/' . $emailConfirmation->code . '/' . $user->email
                ]);
                $this->flashSession->success("Email konfirmasi berhasil dikirim");
                return $this->response->redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }

    /**
     * Starts a session in the admin backend
     */
    public function loginAction() {
        $form = new LoginForm();
        try {
            if (!$this->request->isPost()) {
                if ($this->auth->hasRememberMe()) {
                    return $this->auth->loginWithRememberMe();
                }
            } else {
                $this->auth->check([
                    'email' => $this->request->getPost('email'),
                    'password' => $this->request->getPost('password')
                ]);
                return $this->response->redirect('/');
            }
        } catch (AuthException $e) {
            $this->flash->error($e->getMessage());
        }
        $this->view->form = $form;
    }

    public function ajax_loginAction() {

        if (!$this->request->isPost()) {
            echo json_encode(['success' => false]);
            die;
        }

        $data = $this->auth->check_ajax(array(
            'email' => $this->request->getPost('email', 'email'),
            'password' => $this->request->getPost('password', 'striptags'),
            'remember' => 1
        ));
        if (is_array($data)) {
            $next = $data['console'] === true ? '/console/' : '/my-account';
            $result = array(
                "success" => true,
                "status" => "login",
                "next" => $next
            );
            echo json_encode($result);
            die;
        } elseif ($data == 'banned') {
            $result = array(
                "status" => "banned",
                "success" => false,
                "msg" => "Your account is banned! Pls contact Carijasa Support"
            );
            echo json_encode($result);
            die;
        } else {
            $result = array(
                "status" => $data,
                "success" => false
            );
            echo json_encode($result);
            die;
        }
    }

    /**
     * Shows the forgot password form
     */
    public function forgotPasswordAction() {
        $form = new ForgotPasswordForm();
        if ($this->request->isPost()) {
// Send emails only is config value is set to true
            if ($this->getDI()->get('config')->useMail) {
                if ($form->isValid($this->request->getPost()) == false) {
                    foreach ($form->getMessages() as $message) {
                        $this->flash->error($message);
                    }
                } else {
                    $user = TblUsers::findFirstByEmail($this->request->getPost('email'));
                    if (!$user) {
                        $this->flash->success('There is no account associated to this email');
                    } else {
                        $resetPassword = new ResetPasswords();
                        $resetPassword->usersId = $user->id;
                        if ($resetPassword->save()) {
                            $this->flash->success('Success! Please check your messages for an email reset password');
                        } else {
                            foreach ($resetPassword->getMessages() as $message) {
                                $this->flash->error($message);
                            }
                        }
                    }
                }
            } else {
                $this->flash->warning('Emails are currently disabled. Change config key "useMail" to true to enable emails.');
            }
        }
        $this->view->form = $form;
    }

    /**
     * Closes the session
     */
    public function logoutAction() {
        $this->auth->remove();
        return $this->response->redirect('/');
    }

}
