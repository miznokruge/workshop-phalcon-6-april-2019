<?php
namespace Dkm\Controllers;
use Dkm\Util\Util;
use Dkm\Models\TblBooks;
use Dkm\Models\TblCategory;
use Dkm\Models\TblAuthor;
use Phalcon\Mvc\Url;
use Phalcon\Paginator\Adapter\Model as Paginator;
/**
 * Display the default index page.
 */
class AuthorsController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
        $this->view->setVar('logged_in', is_array($this->auth->getIdentity()));
        
    }

    public function indexAction()
    {

        parent::initialize();
        $this->view->setTemplateBefore('public');
        // $books = TblBooks::findFirstByid($id);
        // $books = TblBooks::find();
        $categories = TblCategory::find();
        $books = TblBooks::find();
         $buku = [];
        foreach ($books as $book) {
            $buku[] = $book->book_title;
        }

       
        $authors = TblAuthor::find();
        $currentPage = $this->request->getQuery('page', 'int', 1);
            $paginator = new Paginator(
        [
            'data'  => $authors,
            'limit' => 24,
            'page'  => $currentPage,
        ]
            );

        // $this->view->books = $books;
        $this->view->page = $paginator->getPaginate();
         $this->view->books = $books;
        $this->view->categories = $categories;

         $this->view->meta = array('d' => 'Semua Judul Buku di Skripsi.co.id '.implode('', $buku), 't' => 'ALL AUTHOR | Skripsi.co.id');

        $this->assets->addCss('css/category/index.css');
    }
    public function detailAction($id)
    {
        $this->view->setTemplateBefore('public');
        $author = TblAuthor::findFirstByauthor_id($id);
        $books = TblBooks::find();
        $book = $author->book;
        
        $data = $this->db->query("select a.author_id,a.author_name as name, SUM(b.book_views) as total_view 
        FROM tbl_books b JOIN tbl_author a ON a.author_id=b.aid
        WHERE a.author_id='".$id."'")->fetchAll()[0];

        $totalrating = $this->db->query("select a.author_id,a.author_name as name, AVG(b.rate_avg) as rate 
        FROM tbl_books b JOIN tbl_author a ON a.author_id=b.aid
        WHERE a.author_id='".$id."'")->fetchAll()[0];

        // $bookviews = $this->db->query("select tbl_author.author_id, tbl_author.author_name, tbl_books.book_title, tbl_books.book_cover_img, tbl_books.book_cover_img, tbl_books.rate_avg, tbl_books.book_views FROM tbl_books JOIN tbl_author ON tbl_author.author_id = tbl_books.aid WHERE tbl_books.aid='".$id."' ORDER BY tbl_books.book_views DESC")->fetchAll();

        // $this->view->bookviews =  TblBooks::find(array("order"=>'book_views DESC','limit'=>3));

         $this->view->bookviews =  TblBooks::find(array("order"=>'book_views DESC','limit'=>3));

        $author->total_views= (int)$data['total_view'];
        $author->total_rating = (int)$totalrating['rate'];
        // $books->book_views = (int)$bookviews['book_views'];
        


        $buku = [];
        foreach ($books as $book) {
            $buku[] = $book->book_title;
        }

        $this->view->author = $author;
        $this->view->books = $books;
        $this->view->meta = array('d' =>$author->author_name.' Menulis Buku '.implode('', $buku), 't' => $author->author_name.'| Skripsi.co.id');
    }
}