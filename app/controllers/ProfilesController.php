<?php

namespace Dkm\Controllers;

use Phalcon\Tag;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Dkm\Forms\ProfilesForm;
use Dkm\Models\Profiles;

/**
 * Dkm\Controllers\ProfilesController
 * CRUD to manage profiles
 */
class ProfilesController extends ControllerBase {

    /**
     * Default action. Set the private (authenticated) layout (layouts/private.volt)
     */
    public function initialize() {
        parent::initialize();
        $this->view->setTemplateBefore('public');
    }

    public function send_invitationAction() {
        if (!$this->request->isPost()) {
            die('error');
        }

        $emails = $this->request->getPost('email');
        foreach ($emails as $email) {

            $param = [
                'name' => $email,
                'inviter' => $this->auth->getIdentity()['name'],
                'url' => 'https://'.$_SERVER['HTTP_HOST'].'/index/track_invitation/' . $this->auth->getIdentity()['id'] . '/' . $email];
            $this->mail->send([$email => $email], ucwords($this->auth->getIdentity()['name']) . " mengundangmu di skripsi.co.id", 'invite', $param);
        }
        $this->flashSession->success("Undangan berhasil dikirim");
        return $this->response->redirect('/profiles');
    }

    /**
     * Default action, shows the search form
     */
    public function indexAction() {
        $this->view->user = \Dkm\Models\TblUsers::findFirstByid($this->auth->getIdentity()['id']);
        $this->assets->addCss('css/profiles/profiles.css');
        $this->assets->addJs('js/profiles/profiles.js');
    }

    /**
     * Searches for profiles
     */
    public function searchAction() {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'Dkm\Models\Profiles', $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = [];
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $profiles = Profiles::find($parameters);
        if (count($profiles) == 0) {

            $this->flash->notice("The search did not find any profiles");

            return $this->dispatcher->forward([
                        "action" => "index"
            ]);
        }

        $paginator = new Paginator([
            "data" => $profiles,
            "limit" => 10,
            "page" => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Creates a new Profile
     */
    public function createAction() {
        if ($this->request->isPost()) {

            $profile = new Profiles([
                'name' => $this->request->getPost('name', 'striptags'),
                'active' => $this->request->getPost('active')
            ]);

            if (!$profile->save()) {
                $this->flash->error($profile->getMessages());
            } else {
                $this->flash->success("Profile was created successfully");
            }

            Tag::resetInput();
        }

        $this->view->form = new ProfilesForm(null);
    }

    /**
     * Edits an existing Profile
     *
     * @param int $id
     */
    public function editAction($id) {
        $profile = Profiles::findFirstById($id);
        if (!$profile) {
            $this->flash->error("Profile was not found");
            return $this->dispatcher->forward([
                        'action' => 'index'
            ]);
        }

        if ($this->request->isPost()) {

            $profile->assign([
                'name' => $this->request->getPost('name', 'striptags'),
                'active' => $this->request->getPost('active')
            ]);

            if (!$profile->save()) {
                $this->flash->error($profile->getMessages());
            } else {
                $this->flash->success("Profile was updated successfully");
            }

            Tag::resetInput();
        }

        $this->view->form = new ProfilesForm($profile, [
            'edit' => true
        ]);

        $this->view->profile = $profile;
    }

    /**
     * Deletes a Profile
     *
     * @param int $id
     */
    public function deleteAction($id) {
        $profile = Profiles::findFirstById($id);
        if (!$profile) {

            $this->flash->error("Profile was not found");

            return $this->dispatcher->forward([
                        'action' => 'index'
            ]);
        }

        if (!$profile->delete()) {
            $this->flash->error($profile->getMessages());
        } else {
            $this->flash->success("Profile was deleted");
        }

        return $this->dispatcher->forward([
                    'action' => 'index'
        ]);
    }

}
