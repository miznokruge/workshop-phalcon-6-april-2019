<?php

namespace Dkm\Controllers;

use Dkm\Util\Util;
use Dkm\Models\Category;
use Dkm\Models\Posts;
use Dkm\Models\Mosque;
use Phalcon\Mvc\Url;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Dkm\Models\Kota;
use Dkm\Models\Desa;
use Dkm\Models\Kecamatan;

/**
 * Display the default index page.
 */
class DashboardController extends ControllerBase {

    public function initialize() {
        if (!is_array($this->auth->getIdentity())) {
            return $this->response->redirect('/');
        }
        //jika punya masjid,arahkan ke halaman pengelolaan masjid
        if (count($this->auth->getIdentity()['mosques']) > 0) {
            return $this->response->redirect('/console');
        }
    }

    public function indexAction() {
        $this->view->setTemplateBefore('dashboard');
        $this->tag->setTitle("Dashboard | Admin");
    }

    public function addlistingAction() {

        if ($this->request->isPost()):

            $m = new Mosque();
            $m->category_id = $this->request->getPost('category_id');
            $m->city_id = $this->request->getPost('city_id');
            $m->user_id = $this->auth->getIdentity()['id'];
            $m->description = $this->request->getPost('description');
            $m->name = $this->request->getPost('name');
            $m->phone = $this->request->getPost('phone');
            $m->videourl = $this->request->getPost('videourl');
            $m->website = $this->request->getPost('website');
            $m->location = $this->request->getPost('location');
            $m->slug = $this->Util->SeoUrl($this->request->getPost('name') . ' ' . $this->request->getPost('location'));
            $m->latlong = $this->request->getPost('lat') . ',' . $this->request->getPost('long');
            if (!$m->save()) {
                $this->debug($m->getMessages());
                die('not saved');
            }
            
            $this->auth->remove();
            $this->auth->authByUserId($m->user_id);

            $this->flashSession->success("Masjid " . $m->name . " berhasil dimasukkan!");
            return $this->response->redirect('/dkm/' . $m->id . '/' . $m->slug);
        endif;

        $this->assets->addJs('https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=' . $this->config->google->map_api_key);
        $this->assets->addJs('/pengurus/vendor/select2/select2.min.js');
        $this->assets->addCss('/pengurus/vendor/select2/select2.css');
        $this->assets->addJs('/js/dashboard/addlisting.js');
        $this->view->kota = Kota::find(['order' => 'urutan asc']);
        $this->view->categories = $this->cache->remember("INDEX_MOSQUE_CATEGORY", 60, function() {
            return Category::find();
        });
    }

    public function getKecamatanAction($idkab) {
        $kec = Kecamatan::find(["id_kabupaten='" . $idkab . "'"]);
        $options = '<option value="">--Pilih--</option>';
        foreach ($kec as $k) {
            $options .= '<option value="' . $k->id . '">' . $k->nama . '</option>';
        }
        die($options);
    }

    public function getDesaAction($idKecamatan) {
        $desa = Desa::find(["id_kecamatan='" . $idKecamatan . "'"]);
        $options = '<option value="">--Pilih--</option>';
        foreach ($desa as $k) {
            $options .= '<option value="' . $k->id . '" data-name="' . $k->nama . ' ' . $k->kecamatan->nama . ' ' . $k->kecamatan->kota->name . '">' . $k->nama . '</option>';
        }
        die($options);
    }

}
