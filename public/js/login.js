$("#public-login-form").submit(function (event) {
    var loader = '<div id="loader-popup" style="position: fixed; top: 0; left: 0; bottom: 0; right: 0; background: rgba(0,0,0,0.8); z-index: 9999;">\n\
<img class="loader" src="/images/loader.gif" style="position: fixed; top: 50%; left: 50%; transform: translate(-50%,-50%);"></div>';
    $('body').append(loader);
    var values = $(this).serializeArray();
    $.post('/session/ajax_login', $.param(values), function (result) {
        //result.replace(/(?:\r\n|\r|\n)/g, '');
        if (result.status === "login") {
            $("#loader-popup, .error-msg").remove();
            window.location.href = result.next;
        } else {
            $("#loader-popup, .error-msg").remove();
            var msgError = '<div class="alert alert-danger alert-dismissible error-msg" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Email / Kata sandi yang Anda masukkan salah</div>'
            $("#listar-loginsingup").find("#public-login-form").before(msgError);
            setTimeout(function () {
                $(".error-msg").remove();
            }, 3000);
        }
    }, 'json');
    return false;
});

$("#login-pengurus").submit(function (event) {
    var loader = '<div id="loader-popup" style="position: fixed; top: 0; left: 0; bottom: 0; right: 0; background: rgba(0,0,0,0.8); z-index: 9999;">\n\
<img class="loader" src="/images/loader.gif" style="position: fixed; top: 50%; left: 50%; transform: translate(-50%,-50%);"></div>';
    $('body').append(loader);
    var values = $(this).serializeArray();
    $.post('/session/ajax_login', $.param(values), function (result) {
        //result.replace(/(?:\r\n|\r|\n)/g, '');
        if (result.status === "login") {
            $("#loader-popup, .error-msg").remove();
            window.location.href = result.next;
        } else {
            $("#loader-popup, .error-msg").remove();
            var msgError = '<div class="alert alert-danger alert-dismissible error-msg" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Email / Kata sandi yang Anda masukkan salah</div>'
            $("#login-pengurus-page").find("#login-pengurus").before(msgError);
            setTimeout(function () {
                $(".error-msg").remove();
            }, 3000);
        }
    }, 'json');
    return false;
});

$("#public-signup-form").submit(function (event) {
    var loader = '<div id="loader-popup" style="position: fixed; top: 0; left: 0; bottom: 0; right: 0; background: rgba(0,0,0,0.8); z-index: 9999;">\n\
<img class="loader" src="/images/loader.gif" style="position: fixed; top: 50%; left: 50%; transform: translate(-50%,-50%);"></div>';
    $('body').append(loader);
    var values = $(this).serializeArray();
    $.post('/session/ajax_signup', $.param(values), function (result) {
        //result.replace(/(?:\r\n|\r|\n)/g, '');
        console.log(result);
        if (result.success === true) {
            $("#loader-popup, .error-msg").remove();
            var msgError = '<div class="alert alert-success" role="alert">\n\
                Pendaftaran berhasil. Cek email Anda untuk mengaktifkan akun Anda</div>'
            $("#listar-loginsingup").find("#public-signup-form").before(msgError);
            setTimeout(function () {
                window.location.href = result.next;
            }, 3000);            
        } else {
            $("#loader-popup, .error-msg").remove();
            var msgError = '<div class="alert alert-danger alert-dismissible error-msg" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Email / Kata sandi yang Anda masukkan salah</div>'
            $("#listar-loginsingup").find("#public-signup-form").before(msgError);
            setTimeout(function () {
                $(".error-msg").remove();
            }, 3000);
        }
    }, 'json');
    return false;
});