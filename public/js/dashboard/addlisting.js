$(document).ready(function (e) {

    $('body').on('change', '#city_id', function (e) {
        var x = $('#city_id').val();
        $.get('/dashboard/getKecamatan/' + x, function (y) {
            $("#kec_id").html(y);
            $("#kec_id").select2();
        });
    });

    $('body').on('change', '#kec_id', function (e) {
        var x = $('#kec_id').val();
        $.get('/dashboard/getDesa/' + x, function (y) {
            $("#desa_id").html(y);
            $("#desa_id").select2();
        });
    });

    // init map
    function initMap(lat, long) {
        var center = new google.maps.LatLng(parseFloat(lat), long);
        var mapOptions = {center: center, zoom: 16, scrollwheel: false};
        map = new google.maps.Map(document.getElementById("register-form__map"), mapOptions);
        marker = new google.maps.Marker({position: new google.maps.LatLng(lat, long), draggable: true, map: map, title: 'Test'});
        google.maps.event.addListener(marker, 'dragend', function (event) {
            var lat = this.getPosition().lat();
            var long = this.getPosition().lng();
            initMap(lat, long);
            $('.register-form__latitude-holder').val(lat);
            $('.register-form__longitude-holder').val(long);
        });
    }
    /**
     * Geocode when user location input changes
     */
    $('body').on('change', '#desa_id', function (e) {
        var address = $(this).find(':selected').data('name');
        var geocoder = new google.maps.Geocoder();
        if (geocoder) {
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    console.log(results[0].geometry.location);
                    var lat = results[0].geometry.location.lat();
                    var long = results[0].geometry.location.lng();
                    console.log("lat=" + lat);

                    initMap(lat, long);
                    $('.register-form__latitude-holder').val(lat);
                    $('.register-form__longitude-holder').val(long);
                } else {
                    alert("Kunne ikke finne denne adressen, vennligst skriv en i nærheten og dra pin'en på kartet nærmest mulig riktig posisjon.");
                    $('.register-form__latitude-holder').focus().select();
                }
            });
        }
        $("#location").val(address);
        $("#long-location").val(address);
    });

    var lat = $('.register-form__latitude-holder').val();
    var long = $('.register-form__longitude-holder').val();
    initMap(lat, long);
    $(".select2").select2();
});
	