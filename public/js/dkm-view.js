$("#share").jsSocials({
    showLabel: true,
    showCount: true,
    shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon", "whatsapp"]
});
